import os
import csv


def lecture_fichier(filepath, delimiter='|'):
    rows=[]
    with open(filepath, 'r') as csvin:
        reader = csv.DictReader(csvin, delimiter=delimiter)
        for row in reader:
            rows.append(row)

def ecriture_fichier(filepath, rows, fieldnames, delimiter='|'):
    with open(filepath, 'w', newline='') as csvout:
        writer = csv.DictWriter( csvout, fieldnames=fieldnames, delimiter=delimiter)
        writer.writeheader()
        writer.writerows(rows)

def modification_nom (row, configuration):
    new_row = {}
    for cle, attr in configuration.items():
        new_row[cle] = row[attr]
    new_row['addresse'] = new_row['addresse'].upper()
    return new_row

def modification_noms (row, configuration):

    return [modification_nom(row, configuration) for row in rows]



