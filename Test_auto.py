import unittest
import os
import csv
from fichier_voiture import ecriture_fichier

class Test_auto(unittest.TestCase):

    CSV_CONFIG = {
     'adresse' : 'address',
     'carro' : 'carrosserie'
        #insérer tout les champs de notre csv d'entrée
     
    }

    CSV_IN_FILEPATH = 'auto.csv'
    CSV_IN_DATA = [
        {
           'address' : '3822 Omar Square Suite 257 Port Emily, OK 43251',
           'carrosserie' : '45-1743376',
           #insérer tout les champs pour cette exemple
        },
        """
        {
            #insérer un autre champ entier
        }
        """

    ]
    CSV_IN_FIELDSNAME = ['address', 'carrosserie',]

    CSV_OUT_FILEPATH = 'sortie_auto.csv'
    CSV_OUT_DATA = [
        {
            'addresse' : '3822 Omar Square Suite 257 Port Emily, OK 43251',
            'carro' : '45-1743376',
            #insérer tout les champs pour cette exemple
        }
        {
            #insérer un autre champ entier
        }
    ]
    CSV_OUT_FIELDSNAME = ['addresse', 'carro',]

    def setUp(self):
        ecriture_fichier(Test_auto.CSV_IN_FILEPATH,
                         Test_auto.CSV_IN_DATA,
                         Test_auto.CSV_IN_FIELDSNAME,
                         delimiter='|')

    def tearDown(self):

        os.remove(Test_auto.CSV_IN_FILEPATH)


    """
    def test_modification_nom(self):
        self.assertEqual(modification_nom("name"),"nom")
        self.assertEqual(modification_nom("energy"),"energie")
        self.assertEqual(modification_nom("firstname"),"prenom")
        self.assertEqual(modification_indexe("date_immat"),"date_immatriculation")

    def test_modification_indexe(self):
        self.assertEqual(modification_indexe(5),12)
        self.assertEqual(modification_indexe(2),10)
        self.assertEqual(modification_indexe(9),3)
        self.assertEqual(modification_indexe(12),2)

    def test_modification_delimiteur(self):
        self.assertEqual(modification_delimiteur("|"),";")

    def test_division_cellule(self):
        self.assertEqual(division_cellule(""),)
        self.assertEqual(division_cellule(),)
    """
        