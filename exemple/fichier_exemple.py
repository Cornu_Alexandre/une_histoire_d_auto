import csv 

"""on remplace w par a pour eviter d'écraser les données"""
with open('exemple.csv', 'a') as csvfile: 
    ecriture = csv.writer(csvfile,delimiter=',')
    ecriture.writerow(['KIKOU-Drag8','Oskoureux','54.01','2021-12-25'])


with open('exemple.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        print(row)

